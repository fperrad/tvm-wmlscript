#!/usr/bin/env perl

=head1 WMLScript Lang library

=head2 Synopsis

    % prove t/lang.t

=head2 Description

Tests WMLScript Lang Library

=cut

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin";

use Test::More tests => 8;
require Helpers;

wmls_is(<<'CODE', <<'OUT', 'Lang.abs');
extern function main()
{
    var a = -3;
    var b = Lang.abs(a);

    Console.println(b);
    Console.println(typeof b);
}
CODE
3
0
OUT

wmls_is(<<'CODE', <<'OUT', 'Lang.abs');
extern function main()
{
    var a = Lang.abs(-3.14);
    Console.println(a);
    Console.println(typeof a);

    a = Lang.abs("text");
    Console.println(typeof a);

    a = Lang.abs(true);
    Console.println(typeof a);

    a = Lang.abs(invalid);
    Console.println(typeof a);
}
CODE
3.14
1
4
4
4
OUT

wmls_is(<<'CODE', <<'OUT', 'Lang.float');
extern function main()
{
    var a = Lang.float();
    Console.println(a);
    Console.println(typeof a);
}
CODE
true
3
OUT

wmls_is(<<'CODE', <<'OUT', 'Lang.exit(0)');
extern function main()
{
    Console.println("exit");
    Lang.exit(0);
    Console.println("ko");
}
CODE
exit
OUT

wmls_is(<<'CODE', <<'OUT', 'Lang.exit("1")');
extern function main()
{
    Console.println("exit");
    Lang.exit("1");
    Console.println("ko");
}
CODE
exit
OUT

wmls_is(<<'CODE', <<'OUT', 'Lang.exit(invalid)');
extern function main()
{
    Console.println("exit");
    Lang.exit(invalid);
    Console.println("ko");
}
CODE
exit
OUT

wmls_like(<<'CODE', <<'OUT', 'Lang.abort');
extern function main()
{
    Lang.abort("abort");
    Console.println("ko");
}
CODE
/abort$/
OUT

wmls_is(<<'CODE', <<'OUT', 'Lang.characterSet');
extern function main()
{
    var a = Lang.characterSet();
    Console.println(a);
    Console.println(typeof a);
}
CODE
4
0
OUT

