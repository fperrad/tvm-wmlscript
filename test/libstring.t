#!/usr/bin/env perl

=head1 WMLScript String library

=head2 Synopsis

    % prove t/libstring.t

=head2 Description

Tests WMLScript String Library

=cut

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin";

use Test::More tests => 14;
require Helpers;

wmls_is(<<'CODE', <<'OUT', 'String.length');
extern function main()
{
    var a = "ABC";
    var b = String.length(a);
    Console.println(b);
    Console.println(typeof b);

    var c = String.length("");
    Console.println(c);
    Console.println(typeof c);

    var d = String.length(342);
    Console.println(d);
    Console.println(typeof d);
}
CODE
3
0
0
0
3
0
OUT

wmls_is(<<'CODE', <<'OUT', 'String.length');
extern function main()
{
    var b = String.length(3.14);
    Console.println(b);
    Console.println(typeof b);

    var c = String.length(false);
    Console.println(c);
    Console.println(typeof c);

    var d = String.length(invalid);
    Console.println(typeof d);
}
CODE
4
0
5
0
4
OUT

wmls_is(<<'CODE', <<'OUT', 'String.isEmpty');
extern function main()
{
    var a = "Hello";
    var b = "";
    var c = String.isEmpty(a);
    Console.println(c);
    Console.println(typeof c);

    var d = String.isEmpty(b);
    Console.println(d);
    Console.println(typeof d);

    var e = String.isEmpty(true);
    Console.println(e);
    Console.println(typeof e);
}
CODE
false
3
true
3
false
3
OUT

wmls_is(<<'CODE', <<'OUT', 'String.isEmpty');
extern function main()
{
    var b = String.isEmpty(0);
    Console.println(b);
    Console.println(typeof b);

    var c = String.isEmpty(3.14);
    Console.println(c);
    Console.println(typeof c);

    var d = String.isEmpty(invalid);
    Console.println(typeof d);
}
CODE
false
3
false
3
4
OUT

wmls_is(<<'CODE', <<'OUT', 'String.charAt');
extern function main()
{
    var a = "My name is Joe";
    var b = String.charAt(a, 0);
    Console.println(b);
    Console.println(typeof b);

    var c = String.charAt(a, 100);
    Console.println(c);
    Console.println(typeof c);

    var d = String.charAt(34, 0);
    Console.println(d);
    Console.println(typeof d);

    var e = String.charAt(a, "first");
    Console.println(typeof e);
}
CODE
M
2

2
3
2
4
OUT

wmls_is(<<'CODE', <<'OUT', 'String.charAt');
extern function main()
{
    var a = "string";
    var b = String.charAt(a, -2);
    Console.println(b);
    Console.println(typeof b);

    var c = String.charAt(a, 3.14);
    Console.println(c);
    Console.println(typeof c);

    var d = String.charAt(a, true);
    Console.println(typeof d);

    var e = String.charAt(a, invalid);
    Console.println(typeof e);
}
CODE

2
i
2
4
4
OUT

wmls_is(<<'CODE', <<'OUT', 'String.charAt');
extern function main()
{
    var b = String.charAt(true, 0);
    Console.println(b);
    Console.println(typeof b);

    var c = String.charAt(3.14, 1);
    Console.println(c);
    Console.println(typeof c);

    var f = String.charAt(invalid, 1);
    Console.println(typeof f);
}
CODE
t
2
.
2
4
OUT

wmls_is(<<'CODE', <<'OUT', 'String.subString');
extern function main()
{
    var a = "ABCD";
    var b = String.subString(a, 1, 2);
    Console.println(b);
    Console.println(typeof b);

    var c = String.subString(a, 2, 5);
    Console.println(c);
    Console.println(typeof c);

    var d = String.subString(1234, 0, 2);
    Console.println(d);
    Console.println(typeof d);
}
CODE
BC
2
CD
2
12
2
OUT

wmls_is(<<'CODE', <<'OUT', 'String.subString');
extern function main()
{
    var a = "ABCD";
    var b = String.subString(a, -1, 2);
    Console.println(b);
    Console.println(typeof b);

    var c = String.subString(a, 10, 2);
    Console.println(c);
    Console.println(typeof c);

    var d = String.subString(a, 0, 0);
    Console.println(d);
    Console.println(typeof d);

    var e = String.subString(a, 0, -2);
    Console.println(e);
    Console.println(typeof e);
}
CODE
AB
2

2

2

2
OUT

wmls_is(<<'CODE', <<'OUT', 'String.find');
extern function main()
{
    var a = "abcde";
    var b = String.find(a, "cd");
    Console.println(b);
    Console.println(typeof b);

    var c = String.find(34.2, "de");
    Console.println(c);
    Console.println(typeof c);

    var d = String.find(a, "qz");
    Console.println(d);
    Console.println(typeof d);

    var e = String.find(34, "3");
    Console.println(e);
    Console.println(typeof e);

    var f = String.find(a, "");
    Console.println(typeof f);
}
CODE
2
0
-1
0
-1
0
0
0
4
OUT

wmls_is(<<'CODE', <<'OUT', 'String.trim');
extern function main()
{
    var a = "Hello";
    var b = " Bye   Jon .  See you!  ";

    var c = String.trim(a);
    Console.println(c);
    Console.println(typeof c);

    var d = String.trim(b);
    Console.println(d);
    Console.println(typeof d);
}
CODE
Hello
2
Bye   Jon .  See you!
2
OUT

wmls_is(<<'CODE', <<'OUT', 'String.compare');
extern function main()
{
    var a = "Hello";
    var b = "Hello";
    var c = String.compare(a, b);
    Console.println(c);
    Console.println(typeof c);

    var d = String.compare("Bye", "Jon");
    Console.println(d);
    Console.println(typeof d);

    var e = String.compare("Jon", "Bye");
    Console.println(e);
    Console.println(typeof e);
}
CODE
0
0
-1
0
1
0
OUT

wmls_is(<<'CODE', <<'OUT', 'String.toString');
extern function main()
{
    var a = String.toString(12);
    Console.println(a);
    Console.println(typeof a);

    var b = String.toString(true);
    Console.println(b);
    Console.println(typeof b);
}
CODE
12
2
true
2
OUT

wmls_is(<<'CODE', <<'OUT', 'String.toString');
extern function main()
{
    var a = String.toString("str");
    Console.println(a);
    Console.println(typeof a);

    var b = String.toString("");
    Console.println(b);
    Console.println(typeof b);

    var c = String.toString(3.14);
    Console.println(c);
    Console.println(typeof c);

    var d = String.toString(invalid);
    Console.println(d);
    Console.println(typeof d);
}
CODE
str
2

2
3.14
2
invalid
2
OUT

