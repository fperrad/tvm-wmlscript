
WMLScript Bytecode Translator
=============================

[![Build Status](https://travis-ci.org/fperrad/tvm-wmlscript.png)](https://travis-ci.org/fperrad/tvm-wmlscript)

The language WMLScript is a subset of ECMAScript (ie JavaScript).
WMLScript defines also an bytecode interpreter (stack based), a binary
format.

WMLScript is a part of the [Wireless Application Protocol](
http://www.openmobilealliance.org/Technical/Affiliates.aspx) specifications.

A WMLScript Compiler is available on [CPAN](
http://search.cpan.org/~perrad/WAP-wmls/).

This translator transforms WMLScript bytecode into TP language
which could executed by the [TvmJIT](http://github.com/fperrad/tvmjit).

The sources are hosted at [http://github.com/fperrad/tvm-wmlscript](
http://github.com/fperrad/tvm-wmlscript).

Copyright and License
---------------------

Copyright (c) 2013 Francois Perrad

This code is licensed under the terms of the MIT/X11 license.
