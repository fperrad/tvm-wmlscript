--
-- tvm-wmlscript : <http://github.com/fperrad/tvm-wmlscript>
--
-- Copyright (c) 2013 Francois Perrad
--
-- This program is licensed under the terms of the MIT/X11 license.
--

local io = require'io'
local math = require'math'
local string = require'string'
local table = require'table'
local tvm = require'tvm'
local assert = assert
local byte = string.byte
local error = error
local floor = math.floor
local format = string.format
local huge = math.huge
local ldexp = math.ldexp
local op = tvm.op.new
local open = io.open
local pairs = pairs
local quote = tvm.quote
local rawget = rawget
local setmetatable = setmetatable
local sub = string.sub
local tconcat = table.concat
local tostring = tostring
local type = type
local unpack = table.unpack or unpack

local stdlibs = require'wmls/runtime'

_ENV = nil

local function slurp (filename)
    local fh = assert(open(filename))
    local content = fh:read'*a'
    fh:close()
    return content
end

local cursor = {}

function cursor.getMB16 (c)
    local n = 0
    local s, i, j = c.s, c.i, c.j
    repeat
        if c.i > c.j then
            c:underflow(i)
            s, i, j = c.s, c.i, c.j
        end
        local b = byte(sub(s, i, i))
        c.i = i+1
        n = n * 0x80 + (b % 0x80)
    until b < 0x80
    return n
end
cursor.getMB32 = cursor.getMB16

function cursor.getUInt8 (c)
    local s, i, j = c.s, c.i, c.j
    if i > j then
        c:underflow(i)
        s, i, j = c.s, c.i, c.j
    end
    local b1 = byte(sub(s, i, i))
    c.i = i+1
    return b1
end

function cursor.getUInt16 (c)
    local s, i, j = c.s, c.i, c.j
    if i+1 > j then
        c:underflow(i+1)
        s, i, j = c.s, c.i, c.j
    end
    local b1, b2 = byte(sub(s, i, i+1), 1, 2)
    c.i = i+2
    return b1 * 0x100 + b2
end

function cursor.getInt8 (c)
    local s, i, j = c.s, c.i, c.j
    if i > j then
        c:underflow(i)
        s, i, j = c.s, c.i, c.j
    end
    local b1 = byte(sub(s, i, i))
    c.i = i+1
    if b1 < 0x80 then
        return b1
    else
        return b1 - 0x100
    end
end

function cursor.getInt16 (c)
    local s, i, j = c.s, c.i, c.j
    if i+1 > j then
        c:underflow(i+1)
        s, i, j = c.s, c.i, c.j
    end
    local b1, b2 = byte(sub(s, i, i+1), 1, 2)
    c.i = i+2
    if b1 < 0x80 then
        return b1 * 0x100 + b2
    else
        return ((b1 - 0xFF) * 0x100 + (b2 - 0xFF)) - 1
    end
end

function cursor.getInt32 (c)
    local s, i, j = c.s, c.i, c.j
    if i+3 > j then
        c:underflow(i+3)
        s, i, j = c.s, c.i, c.j
    end
    local b1, b2, b3, b4 = byte(sub(s, i, i+3), 1, 4)
    c.i = i+4
    if b1 < 0x80 then
        return ((b1 * 0x100 + b2) * 0x100 + b3) * 0x100 + b4
    else
        return ((((b1 - 0xFF) * 0x100 + (b2 - 0xFF)) * 0x100 + (b3 - 0xFF)) * 0x100 + (b4 - 0xFF)) - 1
    end
end

function cursor.getFloat (c)
    local s, i, j = c.s, c.i, c.j
    if i+3 > j then
        c:underflow(i+3)
        s, i, j = c.s, c.i, c.j
    end
    local b4, b3, b2, b1 = byte(sub(s, i, i+3), 1, 4)
    local sign = b1 > 0x7F
    local expo = (b1 % 0x80) * 0x2 + floor(b2 / 0x80)
    local mant = ((b2 % 0x80) * 0x100 + b3) * 0x100 + b4
    if sign then
        sign = -1
    else
        sign = 1
    end
    local n
    if mant == 0 and expo == 0 then
        n = sign * 0.0
    elseif expo == 0xFF then
        if mant == 0 then
            n = sign * huge
        else
            n = 0.0/0.0
        end
    else
        n = sign * ldexp(1.0 + mant / 0x800000, expo - 0x7F)
    end
    c.i = i+4
    return n
end

function cursor.getString (c, n)
    local s, i, j = c.s, c.i, c.j
    local e = i+n-1
    if e > j then
        c:underflow(e)
        s, i, j = c.s, c.i, c.j
    end
    c.i = i+n
    return sub(s, i, e)
end

local mt_cursor = { __index = cursor }

local function mk_cursor_string (src)
    assert(type(src) == 'string')
    return setmetatable({
        s = src,
        i = 1,
        j = #src,
        underflow = function (self)
                        error'missing bytes'
                    end,
    }, mt_cursor)
end

local opcode_mnemo = {
[0]='?-00',
    'JUMP_FW',
    'JUMP_FW_W',
    'JUMP_BW',
    'JUMP_BW_W',
    'TJUMP_FW',
    'TJUMP_FW_W',
    'TJUMP_BW',
    'TJUMP_BW_W',
    'CALL',
    'CALL_LIB',
    'CALL_LIB_W',
    'CALL_URL',
    'CALL_URL_W',
    'LOAD_VAR',
    'STORE_VAR',
    'INCR_VAR',
    'DECR_VAR',
    'LOAD_CONST',
    'LOAD_CONST_W',
    'CONST_0',
    'CONST_1',
    'CONST_M1',
    'CONST_ES',
    'CONST_INVALID',
    'CONST_TRUE',
    'CONST_FALSE',
    'INCR',
    'DECR',
    'ADD_ASG',
    'SUB_ASG',
    'UMINUS',
    'ADD',
    'SUB',
    'MUL',
    'DIV',
    'IDIV',
    'REM',
    'B_AND',
    'B_OR',
    'B_XOR',
    'B_NOT',
    'B_LSHIFT',
    'B_RSSHIFT',
    'B_RSZSHIFT',
    'EQ',
    'LE',
    'LT',
    'GE',
    'GT',
    'NE',
    'NOT',
    'SCAND',
    'SCOR',
    'TOBOOL',
    'POP',
    'TYPEOF',
    'ISVALID',
    'RETURN',
    'RETURN_ES',
    'DEBUG',
    '?-3D',
    '?-3E',
    '?-3F',
    'STORE_VAR_S',
    'STORE_VAR_S',
    'STORE_VAR_S',
    'STORE_VAR_S',
    'STORE_VAR_S',
    'STORE_VAR_S',
    'STORE_VAR_S',
    'STORE_VAR_S',
    'STORE_VAR_S',
    'STORE_VAR_S',
    'STORE_VAR_S',
    'STORE_VAR_S',
    'STORE_VAR_S',
    'STORE_VAR_S',
    'STORE_VAR_S',
    'STORE_VAR_S',
    'LOAD_CONST_S',
    'LOAD_CONST_S',
    'LOAD_CONST_S',
    'LOAD_CONST_S',
    'LOAD_CONST_S',
    'LOAD_CONST_S',
    'LOAD_CONST_S',
    'LOAD_CONST_S',
    'LOAD_CONST_S',
    'LOAD_CONST_S',
    'LOAD_CONST_S',
    'LOAD_CONST_S',
    'LOAD_CONST_S',
    'LOAD_CONST_S',
    'LOAD_CONST_S',
    'LOAD_CONST_S',
    'CALL_S',
    'CALL_S',
    'CALL_S',
    'CALL_S',
    'CALL_S',
    'CALL_S',
    'CALL_S',
    'CALL_S',
    'CALL_LIB_S',
    'CALL_LIB_S',
    'CALL_LIB_S',
    'CALL_LIB_S',
    'CALL_LIB_S',
    'CALL_LIB_S',
    'CALL_LIB_S',
    'CALL_LIB_S',
    'INCR_VAR_S',
    'INCR_VAR_S',
    'INCR_VAR_S',
    'INCR_VAR_S',
    'INCR_VAR_S',
    'INCR_VAR_S',
    'INCR_VAR_S',
    'INCR_VAR_S',
    '?-78',
    '?-79',
    '?-7A',
    '?-7B',
    '?-7C',
    '?-7D',
    '?-7E',
    '?-7F',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_FW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'JUMP_BW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'TJUMP_FW_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
    'LOAD_VAR_S',
}

local opcode_fmt = setmetatable({
    JUMP_FW =           { 'NO_INLINE',  'ARG8' },
    JUMP_FW_W =         { 'NO_INLINE',  'ARG16' },
    JUMP_BW =           { 'NO_INLINE',  'ARG8' },
    JUMP_BW_W =         { 'NO_INLINE',  'ARG16' },
    TJUMP_FW =          { 'NO_INLINE',  'ARG8' },
    TJUMP_FW_W =        { 'NO_INLINE',  'ARG16' },
    TJUMP_BW =          { 'NO_INLINE',  'ARG8' },
    TJUMP_BW_W =        { 'NO_INLINE',  'ARG16' },
    CALL =              { 'NO_INLINE',  'ARG8' },
    CALL_LIB =          { 'NO_INLINE',  'ARG8',         'ARG8' },
    CALL_LIB_W =        { 'NO_INLINE',  'ARG8',         'ARG16' },
    CALL_URL =          { 'NO_INLINE',  'ARG8',         'ARG8',         'ARG8' },
    CALL_URL_W =        { 'NO_INLINE',  'ARG16',        'ARG16',        'ARG8' },
    LOAD_VAR =          { 'NO_INLINE',  'ARG8' },
    STORE_VAR =         { 'NO_INLINE',  'ARG8' },
    INCR_VAR =          { 'NO_INLINE',  'ARG8' },
    DECR_VAR =          { 'NO_INLINE',  'ARG8' },
    LOAD_CONST =        { 'NO_INLINE',  'ARG8' },
    LOAD_CONST_W =      { 'NO_INLINE',  'ARG16' },
    CONST_0 =           { 'NO_INLINE' },
    CONST_1 =           { 'NO_INLINE' },
    CONST_M1 =          { 'NO_INLINE' },
    CONST_ES =          { 'NO_INLINE' },
    CONST_INVALID =     { 'NO_INLINE' },
    CONST_TRUE =        { 'NO_INLINE' },
    CONST_FALSE =       { 'NO_INLINE' },
    INCR =              { 'NO_INLINE' },
    DECR =              { 'NO_INLINE' },
    ADD_ASG =           { 'NO_INLINE', 'ARG8' },
    SUB_ASG =           { 'NO_INLINE', 'ARG8' },
    UMINUS =            { 'NO_INLINE' },
    ADD =               { 'NO_INLINE' },
    SUB =               { 'NO_INLINE' },
    MUL =               { 'NO_INLINE' },
    DIV =               { 'NO_INLINE' },
    IDIV =              { 'NO_INLINE' },
    REM =               { 'NO_INLINE' },
    B_AND =             { 'NO_INLINE' },
    B_OR =              { 'NO_INLINE' },
    B_XOR =             { 'NO_INLINE' },
    B_NOT =             { 'NO_INLINE' },
    B_LSHIFT =          { 'NO_INLINE' },
    B_RSSHIFT =         { 'NO_INLINE' },
    B_RSZSHIFT =        { 'NO_INLINE' },
    EQ =                { 'NO_INLINE' },
    LE =                { 'NO_INLINE' },
    LT =                { 'NO_INLINE' },
    GE =                { 'NO_INLINE' },
    GT =                { 'NO_INLINE' },
    NE =                { 'NO_INLINE' },
    NOT =               { 'NO_INLINE' },
    SCAND =             { 'NO_INLINE' },
    SCOR =              { 'NO_INLINE' },
    TOBOOL =            { 'NO_INLINE' },
    POP =               { 'NO_INLINE' },
    TYPEOF =            { 'NO_INLINE' },
    ISVALID =           { 'NO_INLINE' },
    RETURN =            { 'NO_INLINE' },
    RETURN_ES =         { 'NO_INLINE' },
    DEBUG =             { 'NO_INLINE' },
    STORE_VAR_S =       { 'INLINE4' },
    LOAD_CONST_S =      { 'INLINE4' },
    CALL_S =            { 'INLINE3' },
    CALL_LIB_S =        { 'INLINE3', 'ARG8' },
    INCR_VAR_S =        { 'INLINE3' },
    JUMP_FW_S =         { 'INLINE5' },
    JUMP_BW_S =         { 'INLINE5' },
    TJUMP_FW_S =        { 'INLINE5' },
    LOAD_VAR_S =        { 'INLINE5' },
}, {
    __index = function ()
        return { 'NO_INLINE' }
    end
})

local pc, pc_next, codesize, nvar, constants, functions, nstack, regs, label
local function push (val)
    if regs[nstack] then
        regs[nstack]:push(val)  -- finalize SCAND, SCOR
    else
        regs[nstack] = val
    end
    return 1, false
end
local function unop (meth)
    regs[nstack-1] = op{ '!callmeth1', regs[nstack-1], meth }
    return 0, false
end
local function binop (meth)
    regs[nstack-2] = op{ '!callmeth1', regs[nstack-2], meth, regs[nstack-1] }
    return -1, true
end
local opcode_gen = setmetatable({
    JUMP_FW = function (offset)
        local ip = pc_next + offset
        assert(ip <= codesize, 'VerificationFailed')
        local lbl = 'PC' .. ip
        label[lbl] = true
        return 0, false, op{ '!goto', lbl }
    end,
    JUMP_BW = function (offset)
        local ip = pc - offset
        assert(ip >= 0, 'VerificationFailed')
        local lbl = 'PC' .. ip
        label[lbl] = true
        return 0, false, op{ '!goto', lbl }
    end,
    TJUMP_FW = function (offset)
        local ip = pc_next + offset
        assert(ip <= codesize, 'VerificationFailed')
        local lbl = 'PC' .. ip
        label[lbl] = true
        return -1, true, op{ '!if', op{ '!callmeth1', regs[nstack-1], '_not' }, op{ '!goto', lbl } }
    end,
    TJUMP_BW = function (offset)
        local ip = pc - offset
        assert(ip >= 0, 'VerificationFailed')
        local lbl = 'PC' .. ip
        label[lbl] = true
        return -1, true, op{ '!if', op{ '!callmeth1', regs[nstack-1], '_not' }, op{ '!goto', lbl } }
    end,
    CALL = function (findex)
        local fct = assert(functions[findex], 'VerificationFailed')
        local narg = fct.numberOfArguments
        local call = op{ '!call1', op{ '!index', 'funcs', findex } }
        for i = nstack-narg, nstack-1 do
            call:push(regs[i])
        end
        regs[nstack-narg] = call
        return 1 - narg, narg > 1
    end,
    CALL_LIB = function (findex, lindex)
        local narg = assert(stdlibs.arity[lindex][findex], 'VerificationFailed')
        local call = op{ '!call1', op{ '!index', op{ '!index', 'stdlibs', lindex}, findex } }
        for i = nstack-narg, nstack-1 do
            call:push(regs[i])
        end
        regs[nstack-narg] = call
        return 1 - narg, narg > 1
    end,
    CALL_URL = function (urlindex, findex, narg)
        local url = assert(constants[urlindex], 'VerificationFailed')
        assert(type(url) == 'string', 'VerificationFailed')
        local name = assert(constants[findex], 'VerificationFailed')
        assert(type(name) == 'string', 'VerificationFailed')
        local call = op{ '!call1', 'fct' }
        for i = nstack-narg, nstack-1 do
            call:push(regs[i])
        end
        regs[nstack-narg] = op{ '!do', op{ '!let', 'script', op{ '!call1', 'loader', quote(url) } },
                                       op{ '!let', 'code', op{ '!call1', 'tostring', op{ '!callmeth1', 'script', 'translate' } } },
                                       op{ '!let', 't', op{ '!call1', op{ '!call1', 'assert', op{ '!call1', op{ '!index', 'tvm', quote'load' }, 'code' } } } },
                                       op{ '!let', op{ 'narg', 'fct' }, op{ op{ '!call', 'unpack', op{ '!call1', 'assert', op{ '!index', 't', quote(name) }, quote('ExternalFunctionNotFound ' .. name) } } } },
                                       op{ '!call', 'assert', op{ '!eq', 'narg', narg }, quote'InvalidFunctionArguments' },
                                       call }
        return 1 - narg, narg > 1
    end,
    LOAD_VAR = function (vindex)
        assert(vindex < nvar, 'VerificationFailed')
        return push('var' .. vindex)
    end,
    STORE_VAR = function (vindex)
        assert(vindex < nvar, 'VerificationFailed')
        return -1, true, op{ '!assign', 'var' .. vindex, regs[nstack-1] }
    end,
    INCR_VAR = function (vindex)
        assert(vindex < nvar, 'VerificationFailed')
        local var = 'var' .. vindex
        return 0, false, op{ '!assign', var, op{ '!callmeth1', var, 'incr' } }
    end,
    DECR_VAR = function (vindex)
        assert(vindex < nvar, 'VerificationFailed')
        local var = 'var' .. vindex
        return 0, false, op{ '!assign', var, op{ '!callmeth1', var, 'decr' } }
    end,
    LOAD_CONST = function (cindex)
        local cst = assert(constants[cindex], 'VerificationFailed')
        return push(type(cst) == 'string' and quote(cst) or format('%.6g', cst))
    end,
    CONST_0 = function ()
        return push(0)
    end,
    CONST_1 = function ()
        return push(1)
    end,
    CONST_M1 = function ()
        return push(-1)
    end,
    CONST_ES = function ()
        return push('""')
    end,
    CONST_INVALID = function ()
        return push('!nil')
    end,
    CONST_TRUE = function ()
        return push('!true')
    end,
    CONST_FALSE = function ()
        return push('!false')
    end,
    INCR = function ()
        return unop('incr')
    end,
    DECR = function ()
        return unop('decr')
    end,
    ADD_ASG = function (vindex)
        assert(vindex < nvar, 'VerificationFailed')
        local var = 'var' .. vindex
        return -1, true, op{ '!assign', var, op{ '!callmeth1', var, 'add', regs[nstack-1] } }
    end,
    SUB_ASG = function (vindex)
        assert(vindex < nvar, 'VerificationFailed')
        local var = 'var' .. vindex
        return -1, true, op{ '!assign', var, op{ '!callmeth1', var, '_sub', regs[nstack-1] } }
    end,
    UMINUS = function ()
        return unop('uminus')
    end,
    ADD = function ()
        return binop('add')
    end,
    SUB = function ()
        return binop('_sub')
    end,
    MUL = function ()
        return binop('mul')
    end,
    DIV = function ()
        return binop('div')
    end,
    IDIV = function ()
        return binop('idiv')
    end,
    REM = function ()
        return binop('rem')
    end,
    B_AND = function ()
        return binop('band')
    end,
    B_OR = function ()
        return binop('bor')
    end,
    B_XOR = function ()
        return binop('bxor')
    end,
    B_NOT = function ()
        return unop('bnot')
    end,
    B_LSHIFT = function ()
        return binop('lshift')
    end,
    B_RSSHIFT = function ()
        return binop('rsshift')
    end,
    B_RSZSHIFT = function ()
        return binop('rszshift')
    end,
    EQ = function ()
        return binop('eq')
    end,
    LE = function ()
        return binop('le')
    end,
    LT = function ()
        return binop('lt')
    end,
    GE = function ()
        return binop('ge')
    end,
    GT = function ()
        return binop('gt')
    end,
    NE = function ()
        return binop('ne')
    end,
    NOT = function ()
        return unop('_not')
    end,
    SCAND_TJUMP_FW = function ()
        regs[nstack-1] = op{ '!callmeth1', regs[nstack-1], '_and' }
        return -1, false
    end,
    SCOR_TJUMP_FW = function ()
        regs[nstack-1] = op{ '!callmeth1', regs[nstack-1], '_or' }
        return -1, false
    end,
    TOBOOL = function ()
        return 0, false
    end,
    POP = function ()
        local reg = regs[nstack-1]
        return -1, true, type(reg) == 'table' and reg or nil
    end,
    TYPEOF = function ()
        return unop('typeof')
    end,
    ISVALID = function ()
        return unop('isvalid')
    end,
    RETURN = function ()
        return -1, true, op{ '!return', regs[nstack-1] }
    end,
    RETURN_ES = function ()
        return 0, false, op{ '!return', '""' }
    end,
    DEBUG = function ()
        return 0, false
    end,
}, {
    __index = function (t, k)
        return rawget(t, sub(k, 1, -3)) -- trim _S or _W
            or function () error(format("VerificationFailed [%s]", k)) end
    end
})

local mt_AccessDomain = {
    __tostring = function (self)
        return 'AccessDomain ' .. self.accessDomainIndex
    end,
}

local mt_AccessPath = {
    __tostring = function (self)
        return 'AccessPath ' .. self.accessPathIndex
    end,
}

local mt_UserAgentProperty = {
    __tostring = function (self)
        return 'UserAgentProperty ' .. self.propertyNameIndex
            .. ' ' .. self.contentIndex
    end,
}

local mt_UserAgentPropertyScheme = {
    __tostring = function (self)
        return 'UserAgentPropertyScheme ' .. self.propertyNameIndex
            .. ' ' .. self.contentIndex
            .. ' ' .. self.schemeIndex
    end,
}

local Function = {}

function Function:translate ()
    local i = 1
    local code = self.codeArray
    codesize = #code    -- upval

    local function decode ()
        pc = i - 1      -- upval
        local opcode = byte(sub(code, i, i))
        i = i + 1
        local mnemo = opcode_mnemo[opcode]
        local fmt = opcode_fmt[mnemo]
        local t = {}
        for k = 1, #fmt do
            local arg = fmt[k]
            if     arg == 'NO_INLINE' then
                -- nothing
            elseif arg == 'INLINE3' then
                t[#t+1] = opcode % 0x08
            elseif arg == 'INLINE4' then
                t[#t+1] = opcode % 0x10
            elseif arg == 'INLINE5' then
                t[#t+1] = opcode % 0x20
            elseif arg == 'ARG8' then
                local b = byte(sub(code, i, i))
                i = i + 1
                t[#t+1] = b
            elseif arg == 'ARG16' then
                local b1, b2 = byte(sub(code, i, i+1), 1, 2)
                i = i + 2
                t[#t+1] = b1 * 0x100 + b2
            else
                error(format("invalid arg [%s].", arg))
            end
        end
        pc_next = i - 1 -- upval
        return mnemo, t
    end -- decode

    local args = op{}
    local narg = self.numberOfArguments
    nvar = narg + self.numberOfLocalVariables   -- upval
    for i = 0, narg-1 do
        args:push('var' .. i)
    end
    local top = op{ '!lambda', args }
    for i = narg, nvar-1 do
        top:push(op{'!define', 'var' .. i, quote''})
    end
    local maxstack = 0
    nstack = 0  -- upval
    regs = {}   -- upval
    label = {}  -- upval
    while i <= codesize do
        local mnemo, t = decode()
        top:push(op{ '!label', 'PC' .. pc })
        local n, reset, ops
        if mnemo == 'SCAND' or mnemo == 'SCOR' then
            n, reset, ops = opcode_gen[mnemo .. '_' .. decode()]()
        else
            n, reset, ops = opcode_gen[mnemo](unpack(t))
        end
        if reset then
            for k = nstack + n, nstack - 1 do
                regs[k] = nil
            end
        end
        nstack = nstack + n
        assert(nstack >= 0, 'StackUnderflow')
        if maxstack < nstack then
            maxstack = nstack
        end
        top:push(ops)
    end
    top:push(op{ '!label', 'PC' .. pc_next })
    top:push(op{ '!return', quote'' })

    local t = op{}
    for i = 1, #top do
        local v = top[i]
        if type(v) ~= 'table' or v[1] ~= '!label' or label[v[2]] then
            t:push(v)
            if type(v) == 'table' then
                t:push('\n')
            end
        end
    end
    return t
end

local mt_Function = {
    __index = Function,
    __tostring = function (self)
        local t = {}
        local code = self.codeArray
        local i = 1
        local j = #code
        while i <= j do
            local opcode = byte(sub(code, i, i))
            i = i + 1
            local mnemo = opcode_mnemo[opcode]
            local fmt = opcode_fmt[mnemo]
            t[#t+1] = format('%-12s', mnemo)
            for k = 1, #fmt do
                local arg = fmt[k]
                if     arg == 'NO_INLINE' then
                    -- nothing
                elseif arg == 'INLINE3' then
                    t[#t+1] = format('%7u', opcode % 0x08)
                elseif arg == 'INLINE4' then
                    t[#t+1] = format('%7u', opcode % 0x10)
                elseif arg == 'INLINE5' then
                    t[#t+1] = format('%7u', opcode % 0x20)
                elseif arg == 'ARG8' then
                    local b = byte(sub(code, i, i))
                    i = i + 1
                    t[#t+1] = format('%7u', b)
                elseif arg == 'ARG16' then
                    local b1, b2 = byte(sub(code, i, i+1), 1, 2)
                    i = i + 2
                    t[#t+1] = format('%7u', b1 * 0x100 + b2)
                else
                    error(format("invalid arg [%s].", arg))
                end
            end
            t[#t+1] = '\n'
        end
        return 'NumberOfArgument ' .. self.numberOfArguments .. '\n'
            .. 'NumberOfLocalVariables ' .. self.numberOfLocalVariables .. '\n'
            .. 'FunctionSize ' .. #self.codeArray .. '\n'
            .. tconcat(t)
    end,
}

local script = {}

function script:translate ()
    constants = self.constants  -- upval
    functions = self.functions  -- upval
    local funcs = op{ '0:' }
    for i = 0, self.numberOfFunctions-1 do
        funcs:push(self.functions[i]:translate())
    end
    local funcname = op{}
    for name, idx in pairs(self.functionNameTable) do
        local narg = functions[idx].numberOfArguments
        funcname:addkv(quote(name), op{ narg, op{ '!index', 'funcs', idx } })
    end
    return op{ '!do',
                op{ '!let', 'loader', op{ '!call1', 'require', quote'wmls/bytecode' } },
                op{ '!let', 'stdlibs', op{ '!call1', 'require', quote'wmls/runtime' } },
                op{ '!define', 'funcs' },
                op{ '!assign', 'funcs', funcs },
                op{ '!return', funcname } }
end

local mt_script = {
    __index = script,
    __tostring = function (self)
        local t = {}
        for i = 0, self.numberOfConstants-1 do
            local v = self.constants[i]
            local s = format(type(v) == 'string' and '%q' or '%.6g', v)
            t[#t+1] = 'cst' .. i .. ' ' .. s .. '\n'
        end
        local constants = tconcat(t)
        t = {}
        for i = 0, self.numberOfPragmas-1 do
            local v = self.pragmas[i]
            t[#t+1] = 'pragma' .. i .. ' ' .. tostring(v) .. '\n'
        end
        local pragmas = tconcat(t)
        t = {}
        for name, idx in pairs(self.functionNameTable) do
            t[#t+1] = idx .. ' '  .. name .. '\n'
        end
        local functionNames = tconcat(t)
        t = {}
        for i = 0, self.numberOfFunctions-1 do
            local v = self.functions[i]
            t[#t+1] = 'function' .. i .. '\n' .. tostring(v)
        end
        local functions = tconcat(t)
        return '## file: ' .. self.filename .. '\n'
            .. '##\n'
            .. '## Bytecode Header\n'
            .. '##\n'
            .. 'VersionNumber ' .. (1 + floor(self.versionNumber / 16)) .. '.'
                                .. (self.versionNumber % 16) .. '\n'
            .. 'CodeSize ' .. self.codeSize .. '\n'
            .. '## Constant Pool\n'
            .. '##\n'
            .. 'NumberOfConstants ' .. self.numberOfConstants .. '\n'
            .. 'CharacterSet ' .. self.characterSet .. '\n'
            .. constants
            .. '## Pragma Pool\n'
            .. '##\n'
            .. 'NumberOfPragmas ' .. self.numberOfPragmas .. '\n'
            .. pragmas
            .. '## Function Pool\n'
            .. '##\n'
            .. 'NumberOfFunctions ' .. self.numberOfFunctions .. '\n'
            .. '## Function Name Table\n'
            .. 'NumberOfFunctionNames ' .. self.numberOfFunctionNames .. '\n'
           .. functionNames
           .. functions
           .. '## EOF'
    end,
}

local function mk_script (filename)
    local c = mk_cursor_string(slurp(filename))
    local script = {
        filename = filename,
        constants = {},
        pragmas = {},
        functionNameTable = {},
        functions = {},
    }
    script.versionNumber = c:getUInt8()
    if script.versionNumber ~= 0x01 then
        error'incorrect version'
    end
    script.codeSize = c:getMB32()

    script.numberOfConstants = c:getMB16()
    script.characterSet = c:getMB16()
    for i = 0, script.numberOfConstants-1 do
        local constantType = c:getUInt8()
        local cst
        if     constantType == 0 then
            cst = c:getInt8()
        elseif constantType == 1 then
            cst = c:getInt16()
        elseif constantType == 2 then
            cst = c:getInt32()
        elseif constantType == 3 then
            cst = c:getFloat()
        elseif constantType == 4 then
            cst = c:getString(c:getMB32())
        elseif constantType == 5 then
            cst = ''
        elseif constantType == 6 then
            cst = c:getString(c:getMB32())
        else
            error(format("invalid ConstantType [%d].", constantType))
        end
        script.constants[i] = cst
    end

    script.numberOfPragmas = c:getMB16()
    for i = 0, script.numberOfPragmas-1 do
        local pragmaType = c:getUInt8()
        local prg
        if     pragmaType == 0 then
            local accessDomainIndex = c:getMB16()
            prg = setmetatable({
                accessDomainIndex = accessDomainIndex,
            }, mt_AccessDomain)
        elseif pragmaType == 1 then
            local accessPathIndex = c:getMB16()
            prg = setmetatable({
                accessPathIndex = accessPathIndex,
            }, mt_AccessPath)
        elseif pragmaType == 2 then
            local propertyNameIndex = c:getMB16()
            local contentIndex = c:getMB16()
            prg = setmetatable({
                propertyNameIndex = propertyNameIndex,
                contentIndex = contentIndex,
            }, mt_UserAgentProperty)
        elseif pragmaType == 3 then
            local propertyNameIndex = c:getMB16()
            local contentIndex = c:getMB16()
            local schemeIndex = c:getMB16()
            prg = setmetatable({
                propertyNameIndex = propertyNameIndex,
                contentIndex = contentIndex,
                schemeIndex = schemeIndex,
            }, mt_UserAgentPropertyScheme)
        else
            error(format("invalid PragmaType [%d].", pragmaType))
        end
        script.pragmas[i] = prg
    end

    script.numberOfFunctions = c:getUInt8()
    script.numberOfFunctionNames = c:getUInt8()
    for i = 0, script.numberOfFunctionNames-1 do
        local functionIndex = c:getUInt8()
        local name = c:getString(c:getUInt8())
        script.functionNameTable[name] = functionIndex
    end
    for i = 0, script.numberOfFunctions-1 do
        local numberOfArguments = c:getUInt8()
        local numberOfLocalVariables = c:getUInt8()
        local codeArray = c:getString(c:getMB16())
        local fct = setmetatable({
            numberOfArguments = numberOfArguments,
            numberOfLocalVariables = numberOfLocalVariables,
            codeArray = codeArray,
        }, mt_Function)
        script.functions[i] = fct
    end

    return setmetatable(script, mt_script)
end

return mk_script

