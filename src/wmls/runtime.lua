
--
-- tvm-wmlscript : <http://github.com/fperrad/tvm-wmlscript>
--
-- Copyright (c) 2013 Francois Perrad
--
-- This program is licensed under the terms of the MIT/X11 license.
--

local bit = require'bit'
local debug = require'debug'
local io = require'io'
local math = require'math'
local os = require'os'
local string = require'string'
local abs = math.abs
local ceil = math.ceil
local error = error
local exit = os.exit
local find = string.find
local floor = math.floor
local format = string.format
local getmetatable = debug.getmetatable
local len = string.len
local match = string.match
local pow = math.pow
local setmetatable = debug.setmetatable
local sqrt = math.sqrt
local sub = string.sub
local tonumber = tonumber
local type = type

local band = bit.band
local bor = bit.bor
local bxor = bit.bxor
local bnot = bit.bnot
local lshift = bit.lshift
local rshift = bit.rshift
local arshift = bit.arshift

local mt_any = { __index = function () return function () end end }

local number = {}
setmetatable(number, mt_any)
local mt_number = { __index = number }
setmetatable(0, mt_number)
function number:typeof ()
    return self == floor(self) and 0 or 1
end
function number:tobool ()
    return self ~= 0
end
function number:toint ()
    if floor(self) == self then
        return self
    end
end
function number:tofloat ()
    return self
end
function number:tostring ()
    return format('%.6g', self)
end
function number:isvalid ()
    return true
end
function number:incr ()
    return self + 1
end
function number:decr ()
    return self - 1
end
function number:uminus ()
    return - self
end
function number:add (v)
    if type(v) == 'string' then
        return self:tostring():add(v)
    else
        v = v:tofloat()
        if v then
            return self + v
        end
    end
end
function number:_sub (v)
    v = v:tofloat()
    if v then
        return self - v
    end
end
function number:mul (v)
    v = v:tofloat()
    if v then
        return self * v
    end
end
function number:div (v)
    v = v:tofloat()
    if v then
        if v ~= 0 then
            return self / v
        end
    end
end
function number:idiv (v)
    local u = self:toint()
    if u then
        v = v:toint()
        if v and v ~= 0 then
            return floor(u / v)
        end
    end
end
function number:rem (v)
    local u = self:toint()
    if u then
        v = v:toint()
        if v and v ~= 0 then
            return u % v
        end
    end
end
function number:band (v)
    local u = self:toint()
    if u then
        v = v:toint()
        if v then
            return band(u, v)
        end
    end
end
function number:bor (v)
    local u = self:toint()
    if u then
        v = v:toint()
        if v then
            return bor(u, v)
        end
    end
end
function number:bxor (v)
    local u = self:toint()
    if u then
        v = v:toint()
        if v then
            return bxor(u, v)
        end
    end
end
function number:bnot (v)
    local u = self:toint()
    if u then
        return bnot(u)
    end
end
function number:lshift (v)
    local u = self:toint()
    if u then
        v = v:toint()
        if v then
            return lshift(u, v)
        end
    end
end
function number:rsshift (v)
    local u = self:toint()
    if u then
        v = v:toint()
        if v then
            return rshift(u, v)
        end
    end
end
function number:rszshift (v)
    local u = self:toint()
    if u then
        v = v:toint()
        if v then
            return arshift(u, v)
        end
    end
end
function number:eq (v)
    if type(v) == 'string' then
        return self:tostring():eq(v)
    else
        v = v:tofloat()
        if v then
            return self == v
        end
    end
end
function number:le (v)
    if type(v) == 'string' then
        return self:tostring():le(v)
    else
        v = v:tofloat()
        if v then
            return self <= v
        end
    end
end
function number:lt (v)
    if type(v) == 'string' then
        return self:tostring():lt(v)
    else
        v = v:tofloat()
        if v then
            return self < v
        end
    end
end
function number:ge (v)
    if type(v) == 'string' then
        return self:tostring():ge(v)
    else
        v = v:tofloat()
        if v then
            return self >= v
        end
    end
end
function number:gt (v)
    if type(v) == 'string' then
        return self:tostring():gt(v)
    else
        v = v:tofloat()
        if v then
            return self > v
        end
    end
end
function number:ne (v)
    if type(v) == 'string' then
        return self:tostring():ne(v)
    else
        v = v:tofloat()
        if v then
            return self ~= v
        end
    end
end
function number:_not ()
    return self == 0
end
function number:_and (v)
    return self:tobool():_and(v)
end
function number:_or (v)
    return self:tobool():_or(v)
end


local mt_string = getmetatable('')
local _string = mt_string.__index
setmetatable(_string, mt_any)
function _string:typeof ()
    return 2
end
function _string:tobool ()
    return self ~= ''
end
function _string:toint ()
    return tonumber(self):toint()
end
function _string:tofloat ()
    return tonumber(self)
end
function _string:tostring ()
    return self
end
function _string:isvalid ()
    return true
end
function _string:uminus ()
    return self:tofloat():uminus()
end
function _string:add (v)        -- concat
    if v ~= nil then
        return self .. v:tostring()
    end
end
function _string:_sub (v)
    return tonumber(self):_sub(v)
end
function _string:mul (v)
    return tonumber(self):mul(v)
end
function _string:div (v)
    return tonumber(self):div(v)
end
function _string:idiv (v)
    return self:toint():idiv(v)
end
function _string:rem (v)
    return self:toint():rem(v)
end
function _string:band (v)
    return self:toint():band(v)
end
function _string:bor (v)
    return self:toint():bor(v)
end
function _string:bxor (v)
    return self:toint():bxor(v)
end
function _string:bnot ()
    return self:toint():bnot()
end
function _string:lshift (v)
    return self:toint():lshift(v)
end
function _string:rsshift (v)
    return self:toint():rsshift(v)
end
function _string:rszshift (v)
    return self:toint():rszshift(v)
end
function _string:eq (v)
    if v ~= nil then
        return self == v:tostring()
    end
end
function _string:le (v)
    if v ~= nil then
        return self <= v:tostring()
    end
end
function _string:lt (v)
    if v ~= nil then
        return self < v:tostring()
    end
end
function _string:ge (v)
    if v ~= nil then
        return self >= v:tostring()
    end
end
function _string:gt (v)
    if v ~= nil then
        return self > v:tostring()
    end
end
function _string:ne (v)
    if v ~= nil then
        return self ~= v:tostring()
    end
end
function _string:_not ()
    return self == ''
end
function _string:_and (v)
    return self:tobool():_and(v)
end
function _string:_or (v)
    return self:tobool():_or(v)
end

local boolean = {}
setmetatable(boolean, mt_any)
local mt_boolean = { __index = boolean }
setmetatable(true, mt_boolean)
function boolean:typeof ()
    return 3
end
function boolean:tobool ()
    return self
end
function boolean:toint ()
    return self and 1 or 0
end
function boolean:tofloat ()
    return self and 1.0 or 0.0
end
function boolean:tostring ()
    return self and 'true' or 'false'
end
function boolean:isvalid ()
    return true
end
function boolean:incr ()
   return self:toint():incr()
end
function boolean:decr ()
   return self:toint():decr()
end
function boolean:uminus ()
   return self:toint():uminus()
end
function boolean:add (v)
    if type(v) == 'string' then
        return self:tostring():add(v)
    else
        return self:toint():add(v)
    end
end
function boolean:_sub (v)
    return self:toint():_sub(v)
end
function boolean:mul (v)
    return self:toint():mul(v)
end
function boolean:div (v)
    return self:toint():div(v)
end
function boolean:idiv (v)
    return self:toint():idiv(v)
end
function boolean:rem (v)
    return self:toint():rem(v)
end
function boolean:band (v)
    return self:toint():band(v)
end
function boolean:bor (v)
    return self:toint():bor(v)
end
function boolean:bxor (v)
    return self:toint():bxor(v)
end
function boolean:bnot ()
    return self:toint():bnot()
end
function boolean:lshift (v)
    return self:toint():lshift(v)
end
function boolean:rsshift (v)
    return self:toint():rsshift(v)
end
function boolean:rszshift (v)
    return self:toint():rszshift(v)
end
function boolean:eq (v)
    if type(v) == 'string' then
        return self:tostring():eq(v)
    else
        return self:toint():eq(v)
    end
end
function boolean:le (v)
    if type(v) == 'string' then
        return self:tostring():le(v)
    else
        return self:toint():le(v)
    end
end
function boolean:lt (v)
    if type(v) == 'string' then
        return self:tostring():lt(v)
    else
        return self:toint():lt(v)
    end
end
function boolean:ge (v)
    if type(v) == 'string' then
        return self:tostring():ge(v)
    else
        return self:toint():ge(v)
    end
end
function boolean:gt (v)
    if type(v) == 'string' then
        return self:tostring():gt(v)
    else
        return self:toint():gt(v)
    end
end
function boolean:ne (v)
    if type(v) == 'string' then
        return self:tostring():ne(v)
    else
        return self:toint():ne(v)
    end
end
function boolean:_not ()
   return not self
end
function boolean:_and (v)
    return self and v:tobool()
end
function boolean:_or (v)
    return self or v:tobool()
end


local invalid = {}
setmetatable(invalid, mt_any)
local mt_invalid = { __index = invalid }
setmetatable(nil, mt_invalid)
function invalid:typeof ()
    return 4
end
function invalid:tostring ()
    return 'invalid'
end
function invalid:isvalid ()
    return false
end

--
-- standard libraries
--

local arity_Lang = {}
local Lang = {}
arity_Lang[0] = 1
Lang[0] = function (n)         -- abs(number)
    if type(n) == 'number' then
        return abs(n)
    end
end
arity_Lang[9] = 0
Lang[9] = function (n)         -- float()
    return true
end
arity_Lang[10] = 1
Lang[10] = function (n)        -- exit(code)
    if type(n) == 'string' or type(n) == 'number' then
        n = n:toint()
        if n then
            exit(n)
        end
    end
    exit(0)
end
arity_Lang[11] = 1
Lang[11] = function (s)         -- abort(msg)
    error(s:tostring())
end
arity_Lang[14] = 0
Lang[14] = function (n)         -- characterSet()
    return 4
end

local arity_Float = {}
local Float = {}
arity_Float[0] = 1
Float[0] = function (n)         -- int(number)
    n = n:tofloat()
    if n then
        if n >= 0 then
            return floor(n)
        else
            return ceil(n)
        end
    end
end
arity_Float[1] = 1
Float[1] = function (n)        -- floor(number)
    n = n:tofloat()
    if n then
        return floor(n)
    end
end
arity_Float[2] = 1
Float[2] = function (n)         -- ceil(number)
    n = n:tofloat()
    if n then
        return ceil(n)
    end
end
arity_Float[3] = 2
Float[3] = function (n1, n2)    -- pow(number1, number2)
    n1 = n1:tofloat()
    n2 = n2:tofloat()
    if n1 and n2 then
        return pow(n1, n2)
    end
end
arity_Float[4] = 1
Float[4] = function (n)         -- round(number)
    n = n:tofloat()
    if n then
        return floor(n + 0.5)
    end
end
arity_Float[5] = 1
Float[5] = function (n)         -- sqrt(number)
    n = n:tofloat()
    if n and n >= 0 then
        return sqrt(n)
    end
end

local arity_String = {}
local String = {}
arity_String[0] = 1
String[0] = function (s)        -- length(string)
    if s ~= nil then
        return len(s:tostring())
    end
end
arity_String[1] = 1
String[1] = function (s)        -- isEmpty(string)
    if s ~= nil then
        return s:tostring() == ''
    end
end
arity_String[2] = 2
String[2] = function (s, i)     -- charAt(string, index)
    if type(i) == 'string' then
        i = i:tofloat()
    end
    if s ~= nil and type(i) == 'number' then
        s = s:tostring()
        i = floor(i)
        if i < 0 or i > len(s) then
            return ''
        else
            return sub(s, i+1, i+1)
        end
    end
end
arity_String[3] = 3
String[3] = function (s, i, l)  -- subString(string, startIndex, length)
    if type(i) == 'string' then
        i = i:tofloat()
    end
    if type(l) == 'string' then
        l = l:tofloat()
    end
    if s ~= nil and type(i) == 'number' and type(l) == 'number' then
        s = s:tostring()
        i = floor(i)
        if i < 0 then
            i = 0
        end
        l = floor(l)
        if l < 0 then
            l = 0
        end
        return sub(s, i+1, i+l)
    end
end
arity_String[4] = 2
String[4] = function (s, p)     -- find(string, subString)
    if s ~= nil and p ~= nil then
        s = s:tostring()
        p = p:tostring()
        if len(p) > 0 then
            local i = find(s, p, 1, true)
            return i and i - 1 or -1
        end
    end
end
arity_String[12] = 1
String[12] = function (s)       -- trim(string)
    if s ~= nil then
        return match(s:tostring(), '^%s*(.-)%s*$')
    end
end
arity_String[13] = 2
String[13] = function (s1, s2)  -- compare(string1, string2)
    if s1 ~= nil and s2 ~= nil then
        s1 = s1:tostring()
        s2 = s2:tostring()
        if s1 == s2 then
            return 0
        else
            if s1 < s2 then
                return -1
            else
                return 1
            end
        end
    end
end
arity_String[14] = 1
String[14] = function (s)       -- toString(value)
    return s:tostring()
end

local arity_Console = {}
local Console = {}
arity_Console[0] = 1
Console[0] = function (s)       -- print
    io.write(s:tostring())
end
arity_Console[1] = 1
Console[1] = function (s)       -- println
    io.write(s:tostring())
    io.write('\n')
end

return {
    [0]  = Lang,
    [1]  = Float,
    [2]  = String,
    [99] = Console,
    arity = {
        [0]  = arity_Lang,
        [1]  = arity_Float,
        [2]  = arity_String,
        [99] = arity_Console,
    }
}

