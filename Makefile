
export TVM_PATH=;;src/?.tpc

all: src/wmls/bytecode.tpc src/wmls/runtime.tpc

src/wmls/bytecode.tpc: src/wmls/bytecode.lua
	tvmjit -b $< $@

src/wmls/runtime.tpc: src/wmls/runtime.lua
	tvmjit -b $< $@

test: all
	prove test/*.t

README.html: README.md
	Markdown.pl README.md > README.html

clean:
	rm -f README.html
	rm -f src/wmls/*.tpc

.PHONY: test

